FROM docker.io/library/ubuntu:21.04

RUN apt-get update && apt-get install -y gettext-base

CMD ["/usr/bin/envsubst"]